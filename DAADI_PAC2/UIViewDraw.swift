//
//  UIViewDraw.swift
//  PR2S
//
//  Created by Javier Salvador Calvo on 14/10/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit

class UIViewDraw: UIView {

    var  m_y_axis_min_value:Double = 0
    var  m_y_axis_max_value:Double=0
    var  m_y_axis_step:Double = 0
    
    var  m_x_axis_labels:NSMutableArray?=nil
    var  m_y_values:NSMutableArray?=nil

        
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        // BEGIN-CODE-UOC-3
        setBackgroundColor(screen: rect, color: UIColor.white.cgColor)
        
        let topBarHeight = Double(UIApplication.shared.statusBarFrame.size.height) + 50
        let visibleArea = CGRect(origin: CGPoint(x: 0, y: topBarHeight), size: CGSize(width: rect.width, height: rect.height - CGFloat(topBarHeight)))

        // Initialize variables to draw
        let offset: Double = 50
        let charColor = UIColor.black
        let charLineWidth: CGFloat = 1
        let barsColor = UIColor.blue
        
        // Define chart area
        let chartArea: CGRect = visibleArea.insetBy(dx: CGFloat(offset), dy: CGFloat(offset))
        
        drawAxesLines(area: chartArea, lineWidth: charLineWidth, lineColor: charColor.cgColor)
        
        let yAxisUnitMarks = getYaxisUnitMarks(area: chartArea)
        drawTickMarks(axisUnitMarks: yAxisUnitMarks, horizontal_verticalAxis: true, lineWidth: charLineWidth, lineColor: charColor.cgColor)
        drawLabels(axisUnitMarks: yAxisUnitMarks, horizontal_verticalAxis: true, labelSize: offset, textColor: charColor)
        
        let xAxisUnitMarks = getXaxisUnitMarks(area: chartArea)
        drawTickMarks(axisUnitMarks: xAxisUnitMarks, horizontal_verticalAxis: false, lineWidth: charLineWidth, lineColor: charColor.cgColor)
        drawLabels(axisUnitMarks: xAxisUnitMarks, horizontal_verticalAxis: false, labelSize: offset, textColor: charColor)
        drawBars(area: chartArea, ticks: xAxisUnitMarks, color: barsColor.cgColor)
    }

    // Set the background color
    func setBackgroundColor(screen: CGRect, color: CGColor) {
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {return}
        context.setFillColor(color)
        context.fill(screen)
    }
    
    // Draw the axes lines for this screen size
    func drawAxesLines(area: CGRect, lineWidth: CGFloat, lineColor: CGColor) {
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {return}
        context.setLineWidth(lineWidth)
        context.setStrokeColor(lineColor)
        context.addLines(between: [area.origin, CGPoint(x: area.minX, y: area.maxY), CGPoint(x: area.maxX, y: area.maxY)])
        context.strokePath()
    }
    
    // Collect the labels and points of all x-axis unit marks
    func getXaxisUnitMarks(area: CGRect) -> [String: CGPoint] {
        let distanceXstep = distanceXperCategory(area: area)
        var distanceXfromOrigin = Double(area.minX)
        var ticks: Dictionary = [String: CGPoint]()
        
        for label in (m_x_axis_labels! as NSArray){
            distanceXfromOrigin += distanceXstep
            ticks["\(label)"] = CGPoint(x: distanceXfromOrigin, y: Double(area.maxY))
        }
        return ticks
    }
    
    // Collect the labels and points of all y-axis unit marks
    func getYaxisUnitMarks(area: CGRect) -> [String: CGPoint] {
        let distanceYstep = distanceYperUnit(area: area) * m_y_axis_step
        var distanceYfromOrigin = Double(area.maxY)
        var currentStepValue = m_y_axis_min_value + m_y_axis_step
        var ticks: Dictionary = [String: CGPoint]()
        
        while (currentStepValue <= m_y_axis_max_value) {
            distanceYfromOrigin -= distanceYstep
            ticks[String(Int(currentStepValue))] = CGPoint(x: Double(area.minX), y: distanceYfromOrigin)
            currentStepValue += m_y_axis_step
        }
        return ticks
    }
    
    // Draw the tick marks for the given axis unit marks
    func drawTickMarks(axisUnitMarks: [String: CGPoint], horizontal_verticalAxis: Bool, lineWidth: CGFloat, lineColor: CGColor) {
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {return}
        context.setLineWidth(lineWidth)
        context.setStrokeColor(lineColor)
        let tickWidht = lineWidth * 10
        
        // Define displacement according to it is drawing on x-axis or y-axis
        let horizontalDisplacement: CGFloat = horizontal_verticalAxis ? tickWidht : 0
        let verticalDisplacement: CGFloat = horizontal_verticalAxis ? 0 : tickWidht
        
        for (_, point) in axisUnitMarks {
            let startCurrentTick = CGPoint(x: (point.x - (horizontalDisplacement/2)), y: (point.y - (verticalDisplacement/2)))
            let endCurrentTick = CGPoint(x: (point.x + (horizontalDisplacement/2)), y: (point.y + (verticalDisplacement/2)))
        
            context.move(to: startCurrentTick)
            context.addLine(to: endCurrentTick)
            context.strokePath()
        }
    }
    
    // Draw the labels for the given axis unit marks
    func drawLabels(axisUnitMarks: [String: CGPoint], horizontal_verticalAxis: Bool, labelSize: Double, textColor: UIColor) {
        for (label, point) in axisUnitMarks {
            var labelFrame: CGRect
            if horizontal_verticalAxis {
                // Drawing on y-axis
                labelFrame =  CGRect(x: 0, y: Double(point.y) - (labelSize/2), width: labelSize, height: labelSize)
            } else {
                // Drawing on x-axis
                labelFrame = CGRect(x: Double(point.x) - (labelSize/2), y: Double(point.y), width: labelSize, height: labelSize)
            }
            let currentTickLabel = UILabel(frame: labelFrame)
            currentTickLabel.text = label
            currentTickLabel.textColor = textColor
            currentTickLabel.textAlignment = .center
            currentTickLabel.numberOfLines = 1
            currentTickLabel.font = currentTickLabel.font.withSize(CGFloat(labelSize/3))
            currentTickLabel.adjustsFontSizeToFitWidth = true
            self.addSubview(currentTickLabel)
        }
    }
        
    // Draw bars on the x-axis
    func drawBars(area: CGRect, ticks: [String: CGPoint], color: CGColor) {
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {return}
        context.setFillColor(color)
        
        let barWidth = distanceXperCategory(area: area) - (Double(area.minX)/4)
        let relationYtoValue = distanceYperUnit(area: area)
        for (label, point) in ticks {
            let index = m_x_axis_labels!.index(of: Int(label) ?? 0)
            let yValue =  (m_y_values![index] as! Double) * relationYtoValue
            let bar = CGRect(x: (Double(point.x) - (barWidth/2)), y: (Double(point.y) - yValue), width: barWidth, height: yValue)
            context.fill(bar)
        }
    }
    
    // Get the distance in y-axis for each unit
    func distanceYperUnit(area: CGRect) -> Double {
        return (Double(area.height) / (m_y_axis_max_value - m_y_axis_min_value))
    }
    
    // Get the distance in x-axis for each category
    func distanceXperCategory(area: CGRect) -> Double {
        return (Double(area.width) / Double(m_x_axis_labels!.count + 1))
        // END-CODE-UOC-3
    }
 

}
